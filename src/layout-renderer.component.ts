import {
  Compiler,
  Component,
  EventEmitter,
  Input,
  NgModule,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { JitCompilerFactory } from '@angular/compiler';
import { Http, Response } from '@angular/http';
import 'rxjs';

import { DynamicImportConfig } from './dynamicImportConfig';

@Component({
  selector: 'abs-layout-renderer',
  template: `<ng-template #container></ng-template>`
})
export class LayoutRendererComponent implements OnInit {
  @ViewChild('container', { read: ViewContainerRef }) container: ViewContainerRef;

  @Input() moduleStore: string;
  @Input() templateStore: string;
  @Input() templateId: string;
  @Input() context: any;
  @Output() onEmitted = new EventEmitter<any>();

  private compiler: Compiler = new JitCompilerFactory([{ useDebug: false, useJit: true }]).createCompiler();

  constructor(private vcRef: ViewContainerRef, private _http: Http) { }

  public ngOnInit(): void {
    this.renderTemplateFromStore();
  }

  private renderTemplateFromStore() {
    const templateUrl = this.templateStore + this.templateId;
    const dynamicImports = [];
    this._http.get(templateUrl).map((response: Response) => response.text()).subscribe((html: string) => {
      // parse the dynamic imports from the template
      const allMatches = html.match(/<!-- abs-import \'(.*)#(.*)\' -->/g) || [];
      allMatches.forEach((fullMatch) => {
        const matches = fullMatch.match(/<!-- abs-import \'(.*)#(.*)\' -->/);
        const dynamicImport = new DynamicImportConfig();
        dynamicImport.modulePath = matches[1]; // matches[0] is the complete string match
        dynamicImport.moduleName = matches[2];
        dynamicImports.push(dynamicImport);
      });

      this.loadImportedModules(html, dynamicImports);
    }, (error: any) => {
      console.log('Something went wrong');
    });
  }

  private loadImportedModules(html: string, dynamicImports: DynamicImportConfig[]) {
    const fileToLoad = dynamicImports.map( (config) => this.moduleStore + config.modulePath)[0];
    this.loadScript(fileToLoad, () => {
      const fileContent = window['sample'];
      const importedModules = dynamicImports.map( (config) => {
        const importedModule = NgModule(NgModule(fileContent[config.moduleName].decorators[0].args[0]))(fileContent[config.moduleName]);
        // compile dynamic module
        const modFactory = this.compiler.compileModuleAndAllComponentsSync(importedModule);
        // get moduleType of the dynamically loaded module
        return modFactory.ngModuleFactory.moduleType;
      });
      this.renderTemplate(html, importedModules);
    });
  }

  private loadScript(url: string, callback: any) {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.onload = function(){
        callback();
    };

    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);
  }

  private renderTemplate(template: string, importedModules: any[]) {
    const context = this.context;
    @Component({
      selector: 'abs-dynamic-root',
      template: template,
      // add providers array for services from the importedModule
      // providers: importedModules.map(m => m.forRoot().providers).flatten()
      // todo: make it error safe
      providers: importedModules[0].forRoot().providers
    })
    class DynamicRootComponent {
      public context = context;

      @Output() onEmitted = new EventEmitter<any>();

      public emit(event) {
        this.onEmitted.emit(event);
      }
    };

    @NgModule({
      imports: importedModules.map(m => m.forRoot()),
      declarations: [DynamicRootComponent],
      exports: [DynamicRootComponent]
    })
    class DynamicRootModule { };

    const modFactory = this.compiler.compileModuleAndAllComponentsSync(DynamicRootModule);
    const cmpFactory = modFactory.componentFactories.find(f => f.componentType === DynamicRootComponent);

    const cmpRef = this.container.createComponent(cmpFactory);
    cmpRef.instance.onEmitted.subscribe((event) => {
      this.onEmitted.emit(event);
    });
  }
}
