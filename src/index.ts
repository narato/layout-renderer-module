import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { LayoutRendererComponent } from './layout-renderer.component';
import { ValidationService } from './validation.service';

export * from './layout-renderer.component';
export * from './validation.service';

declare var require: any;

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [
    LayoutRendererComponent
  ],
  exports: [
    LayoutRendererComponent
  ],
  entryComponents: [
    LayoutRendererComponent
  ],
  providers: [
    ValidationService
  ]
})
export class AbsLayoutRendererModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AbsLayoutRendererModule,
      providers: [ValidationService]
    };
  }
}

let win = window as any;
win.global_ng_core = require('@angular/core');
win.global_ng_common = require('@angular/common');
win.global_ng_forms = require('@angular/forms');
