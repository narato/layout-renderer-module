export class DynamicImportConfig {
  moduleName: string;
  modulePath: string;
}
